# Intro Docker #

[![Docker Container](../img/06-docker-container.jpg)](../img/06-docker-container.jpg)

## Instalasi Docker ##

1. Instalasi Docker

    * Tambahkan GPG Key Docker

        ```
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        ```

    * Tambahkan Repository Docker

        ```
        sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"
        ```

    * Update Database Package

        ```
        apt update
        ```

    * Install Docker

        ```
        apt install docker-ce docker-ce-cli containerd.io -y
        ```

2. Test Instalasi 

        docker --version

    Outputnya:

        Docker version 19.03.2, build 6a30dfc

3. Buat folder untuk document root Nginx

        mkdir /opt/nginx-docroot

4. Buat file index.html

        vim /opt/nginx-docroot/index.html

5. Jalankan container Nginx dengan mapping docroot dan port

        docker run -v /opt/nginx-docroot:/usr/share/nginx/html -p 80:80  -d nginx

6. Pastikan containernya sudah jalan

        docker ps -a

    Outputnya :

        CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                NAMES
        c349728925ad        nginx               "nginx -g 'daemon of…"   11 seconds ago      Up 9 seconds        0.0.0.0:80->80/tcp   hungry_kilby

7. Browse ke IP docker host : `http://<ip-address>`

## Cara Membuat Docker Image ##

1. Membuat `Dockerfile`

    * `FROM` : image yang sudah ada, digunakan sebagai dasar/awal pembuatan supaya tidak mulai dari nol
    * `COPY` : copy file dari local folder ke dalam container image
    * `RUN`  : menjalankan perintah
    * `ENTRYPOINT` : perintah yang dieksekusi pada waktu image dijalankan

    Contoh `Dockerfile`

    ```
    FROM openjdk:8
    ADD target/springboot-bukutamu-0.0.1-SNAPSHOT.jar /opt/app.jar
    RUN bash -c 'touch /opt/app.jar'
    ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/opt/app.jar"]
    ```

2. Build image

    ```
    docker build -t endymuhardin/halo-docker-php .
    ```

    Penjelasan :

    * `-t` : nama tag
    * `.` : proses folder tempat kita berada

3. Upload image ke docker registry

    ```
    docker login
    ```

    Outputnya:

    ```
    Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
    Username: endymuhardin
    Password: 
    WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
    Configure a credential helper to remove this warning. See
    https://docs.docker.com/engine/reference/commandline/login/#credentials-store

    Login Succeeded
    ```

    Push image

    ```
    docker push endymuhardin/halo-docker-php
    ```

    Outputnya:

    ```
    The push refers to repository [docker.io/endymuhardin/halo-docker-php]
    49e6d6590b78: Pushed 
    8580babb415b: Mounted from library/php 
    058bad428d20: Mounted from library/php 
    ac4b76a32759: Mounted from library/php 
    a3a36fbddcff: Mounted from library/php 
    8845886127f8: Mounted from library/php 
    285b167deb07: Mounted from library/php 
    ```

4. Jalankan image

    ```
    docker run -p 10000:80 -d endymuhardin/halo-docker-php
    ```

5. Akses console ke container yang sedang berjalan

    ```
    docker exec -it <container name> /bin/bash
    ```

6. Melihat semua container yang ada dalam host

    ```
    docker ps -a
    ```

## Referensi ##

* [Intro Docker](https://software.endy.muhardin.com/linux/intro-docker/)
* [Workflow Development dengan Docker](https://software.endy.muhardin.com/devops/docker-workflow/)
* [Dockerize Aplikasi PHP](http://geekyplatypus.com/making-your-dockerised-php-application-even-better/)
* [Image Docker Yii2](https://github.com/yiisoft/yii2-docker)
* [Konfigurasi Nginx menjadi Front Proxy](https://codereviewvideos.com/course/docker-tutorial-for-beginners/video/docker-nginx-php-tutorial)
* [Kustomisasi PHP Image](https://codereviewvideos.com/course/docker-tutorial-for-beginners/video/docker-php-7-tutorial-7-7-1-and-higher)
* [Konfigurasi PHP-FPM Backend/Upstream](https://codereviewvideos.com/course/docker-tutorial-for-beginners/video/docker-php-symfony-tutorial)
* [Compose Nginx - PHP - MySQL](https://codereviewvideos.com/course/docker-tutorial-for-beginners/video/docker-compose-tutorial)
